# Maintainer: Christian Rebischke <chris.rebischke@archlinux.org>
# Contributor: Arturo Penen <apenen@gmail.com>

pkgname=istio
pkgver=1.19.0
pkgrel=1
pkgdesc='Istio configuration command line utility for service operators to debug and diagnose their Istio mesh.'
arch=('x86_64')
url='https://github.com/istio/istio'
license=('Apache')
makedepends=('go')
source=("$pkgname-$pkgver.tar.gz::https://github.com/istio/istio/archive/$pkgver.tar.gz")
sha512sums=('add0325125215c308c7989839ee33935ec0fec25c2b966fc8e7fcbfae027a2de35c4acff3df05db6a253573c0a23c6594830190a44075fa31a78723f5cad154e')
b2sums=('62847cb72f1c2d9a9fc87258eaa4e6751043e5d55a1f03b3939577bb951abb83ea18b2adfe313214431a63902f7d444753fd411478e3f576c3de7f221e17ff40')

build() {
  cd $pkgname-$pkgver
  export VERSION=$pkgver
  export TAG=$pkgver
  export BUILD_WITH_CONTAINER=0
  make build
}

package() {
  install -Dm 755 "${srcdir}/$pkgname-$pkgver/out/linux_amd64/istioctl" "${pkgdir}/usr/bin/istioctl"

  # Populate bash and zsh completions
  install -dm 755 "${pkgdir}/usr/share/bash-completion/completions"
  install -dm 755 "${pkgdir}/usr/share/zsh/site-functions"
  "${pkgdir}/usr/bin/istioctl" collateral --bash
  mv istioctl.bash "${pkgdir}/usr/share/bash-completion/completions/istioctl"
  "${pkgdir}/usr/bin/istioctl" collateral --zsh
  mv _istioctl  "${pkgdir}/usr/share/zsh/site-functions/_istioctl"
}
